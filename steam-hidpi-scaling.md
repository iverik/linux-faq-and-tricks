# Steam and HiDPI scaling

Navigate to the Steam desktop file and edit in nano or some other editor. 

```
cd ~/.local/share/applications/

sudo nano steam.desktop
```


Find the line with Exec= and add 

```
GDK_SCALE=2 env GDK_DPI_SCALE=0.5
```

It will likely look like this when added:
```
Exec=GDK_SCALE=2 env GDK_DPI_SCALE=0.5 /usr/games/steam %U
```

This will scale Steam 200%, so it'll be huge. But at least it won't be tiny.
