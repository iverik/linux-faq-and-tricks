Possible workaround to create UI button to power down HDD in KDE Plasma.

Create a file called `poweroff.desktop` at `~/.local/share/solid/actions/` with the following content:

    [Desktop Action open]
    Exec=solid-hardware unmount %i &&  udisksctl power-off -b %d
    Icon=kt-stop
    Name=power off device

    [Desktop Entry]
    Actions=open;
    Type=Service
    X-KDE-Action-Custom=true
    X-KDE-Solid-Predicate=[ IS StorageVolume AND StorageVolume.ignored == false ]

Log out and in again. 

> The file creates a new entry for the device notifier in the control panel to power off the device.The command first unmounts the drive and then powers it off. In contrast to OP's experience my external HDD stays off and doesn't restart again.
 
