## Compress and Extract files using the `tar` command on Linux

##### Flags / switches

* `-c`: Create an archive.
* `-z`: Compress the archive with **gzip**.
* `-v`: Display progress in the terminal while creating the archive, also known as “verbose” mode. The v is always optional in these commands, but it’s helpful.
* `-f`: Allows you to specify the filename of the archive.
* `--exclude=`: Excludes the trailing file- or directory path from archiving.

Alternative compression: Use **bzip** (`.tar.bz2`, `.tar.bz`, `.tbz`)
* `-j` instead of `-z`

#### Creating and compressing directory and/or file to archive

##### Compress entire directory of single file

    tar -czvf name-of-archive.tar.gz /path/to/directory-or-file

##### Example of Create (c), Compress (z), Display process (v) (verbose), [File] Name (f) an archive named `MyArchive` with file `MyDirectory` (flags: Create, Compress, Verbose, Name)
    tar -czvf MyArchive.tar.gz MyDirectory

##### Archiving file or directory, located elswhere on the computer (flags: Create, Compress, Verbose, Name)
    tar -czvf MyArchive.tar.gz /usr/local/SomethingSomething


##### Archiving multiple directories or files at once (flags: Create, Compress, Verbose, Name)
    tar -czvf MyArchive.tar.gz /home/ubuntu/Downloads /usr/local/stuff /home/ubuntu/Documents/notes.txt

##### Exclude directories and files
Append --exclude switch for each directory or file you want to exclude.
    
    tar -czvf MyArchive.tar.gz /home/USER --exclude=/home/USER/Downloads --exclude=/home/USER/.cache

#### Extracting an archive

* `-z` for **gzip** (`-xzvf`)
* `-j` for **bzip** (`-xjvf`)
* `-C`: Specify location to extract archive to.

##### Extract to current directory
    tar -xzvf MyArchive.tar.gz

Same as creation commands, except `-x` flag replaces `-c`flag. -x specifies e**x**tract instead of **c**reate.

##### Extract to new directory
    tar -xzvf MyArchive.tar.gz -C /tmp

Appending `-C` flag will extract to specified directory (here: /tmp)



Credit: https://www.howtogeek.com/248780/how-to-compress-and-extract-files-using-the-tar-command-on-linux/
