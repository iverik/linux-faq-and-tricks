# HiDPI Scaling with Spotify and Flatpak.


Install spotify via flatpak using the instructions on flathub.

To force Spotify to scale to something readable, we can use the `--force-device-scale-factor=` command line option (where X is the scaling factor).

E.g. X = 1.6 works pretty well.

To test, you can run 

```
flatpak run com.spotify.Client --force-device-scale-factor=1.6
```

in your terminal.


In order to make this a permanent change, you can edit the desktop file (the one you have in your start menu or panel).



## Option 1: Fancy oneliner:

If you're feeling fancy, you can use a oneliner.
- It will first make a backup of the desktop file with .old at the end of the filename (in this case, `com.spotify.Client.desktop.old`)
- Then it will append the argument at the end of the Exec=xyz line`.
```
sed -i.old '/^Exec=/s/$/ --force-device-scale-factor=1.6/' ~/.local/share/applications/com.spotify.Client.desktop
```


## Option 2: Step-by-step

If you like doing it step by step, instead of fancy oneliners.

Navigate to the Flatpak shortcut directory:
```
~/.local/share/applications/
```


Edit the com.spotify.Client.desktop file as root with your favorite text editor. 

```
sudo nano com.spotify.Client.desktop
```


Append the aforementioned command line argument to the end of the line starting with 'Exec'

```
--force-device-scale-factor=1.6
```


## Result

The entire line would likely look something like this (this is mine):
```
Exec=/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=spotify --file-forwarding com.spotify.Client @@u %U @@ flatpak run com.spotify.Client --force-device-scale-factor=1.6
```

The effects should be seen next time you launch Spotify.

___


Just for reference, you can find the "main" file here:
```
/var/lib/flatpak/exports/share/applications/
```
___

Credits to https://justincaustin.com/blog/spotify-flatpak-hidpi-scaling
